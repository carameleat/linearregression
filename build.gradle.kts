//                                  Bismillahirrahmanirrahim


buildscript {
    repositories {
        jcenter()

    }
    dependencies {
        classpath(embeddedKotlin("gradle-plugin"))
    }
}

plugins {
    kotlin("jvm") version "1.4.10"
    kotlin("kapt") version "1.4.10"
    id("org.openjfx.javafxplugin") version "0.0.9"
}

group = "com.flife"
version = "1.0-SNAPSHOT"

allprojects {
    repositories {
        jcenter()
    }
}