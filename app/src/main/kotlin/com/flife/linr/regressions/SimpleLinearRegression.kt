//                              Bismillahirrahmanirrahim


package com.flife.linr.regressions

import com.flife.linr.models.Case
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import tornadofx.*

class SimpleLinearRegression(
    cases: ObservableList<Case> = observableListOf()
) {

    var cases: ObservableList<Case> = observableListOf()
        set(value) {
            value.addListener(ListChangeListener {
                sigmaX = sigmaOf { it.x!! }
                sigmaXSquare = sigmaOf { it.xSquare }
                sigmaXY = sigmaOf { it.xy }
                sigmaY = sigmaOf { it.y!! }
                sigmaYSquare = sigmaOf { it.ySquare }
                a = ((sigmaY * sigmaXSquare) - (sigmaX * sigmaXY)) /
                    ((n * sigmaXSquare) - (sigmaX * sigmaX))
                b = ((n * sigmaXY) - (sigmaX * sigmaY)) / ((n * sigmaXSquare) - (sigmaX * sigmaX))
            })

            field = value
        }
    var sigmaX = 0.00
    var sigmaXSquare = 0.00
    var sigmaXY = 0.00
    var sigmaY = 0.00
    var sigmaYSquare = 0.00
    val n: Int get() = cases.size
    var a: Double = 0.00
    var b: Double = 0.00

    init {
        this.cases = cases
    }

    fun yOf(x: Double): Double = a + (b * x)

    fun xOf(y: Double): Double = (y - a) / b

    private fun sigmaOf(field: ((case: Case) -> Double)): Double {
        var sigma = 0.0

        cases.forEach {
            sigma += field(it)
        }

        return sigma
    }
}