//                                  Bismillahirrahmanirrahim


package com.flife.linr.models

data class Case(
    var x: Double? = null,
    var y: Double? = null
) {

    val xSquare get() = if (x != null) x!! * x!! else 0.0
    val xy get() = if (x != null && y != null) x!! * y!! else 0.0
    val ySquare get() = if (y != null) y!! * y!! else 0.0
}