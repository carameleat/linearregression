//                              Bismillahirrahmanirrahim


package com.flife.linr.viewmodels

import com.flife.linr.models.Case
import com.flife.linr.regressions.SimpleLinearRegression
import javafx.collections.ObservableList
import tornadofx.ViewModel

class MainViewModel : ViewModel() {

    private val regression = SimpleLinearRegression()
    val sigmaX get() = regression.sigmaX
    val sigmaXSquare get() = regression.sigmaXSquare
    val sigmaXY get() = regression.sigmaXY
    val sigmaY get() = regression.sigmaY
    val sigmaYSquare get() = regression.sigmaYSquare
    val n get() = regression.n
    val a get() = regression.a
    val b get() = regression.b
    var predictedCase: Case? = null

    fun setCases(cases: ObservableList<Case>) {
        regression.cases = cases
    }

    fun addCase(case: Case) {
        regression.cases.add(case)
    }

    fun clearCases() {
        regression.cases.clear()
    }

    fun removeCases(cases: Collection<Case>) {
        regression.cases.removeAll(cases)
    }

    fun xOf(y: Double) = regression.xOf(y)

    fun yOf(x: Double) = regression.yOf(x)
}