//                                  Bismillahirrahmanirrahim


package com.flife.linr

import com.flife.linr.ui.views.MainView
import com.flife.linr.utils.Resource
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import tornadofx.App

class LinRApplication : App(MainView::class) {

    companion object {
        val ICON: ImageView = ImageView(Image(Resource.getDrawable("flife_small.png")))
    }
}