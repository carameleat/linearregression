//                          Bismillahirrahmanirrahim


/*
 * The coroutine parts is from Android viewModelScope extension's source code
 * and licensed under :
 *
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------
 *
 * Insyaallah, you can find the original version in the following link
 *  https://android.googlesource.com/platform/frameworks/support/+/refs/heads/androidx-master-dev/lifecycle/lifecycle-viewmodel-ktx/src/main/java/androidx/lifecycle/ViewModel.kt
 * Then, insyaallah the modification that I have made is to make it suitable with the TornadoFX library.
 *
 */
package com.flife.linr.libs

import javafx.scene.Node
import kotlinx.coroutines.*
import tornadofx.View
import java.io.Closeable
import kotlin.coroutines.CoroutineContext

abstract class View(title: String? = null, icon: Node? = null) : View(title, icon) {

    private var coroutineScope: CoroutineScope? = null

    val viewScope: CoroutineScope get() {
        if(coroutineScope == null) {
            coroutineScope = CloseableCoroutineScope(SupervisorJob() + Dispatchers.Main.immediate)
        }

        return coroutineScope!!
    }

    override fun onUndock() {
        super.onUndock()
        (viewScope as CloseableCoroutineScope).close()
    }

    internal class CloseableCoroutineScope(private val context: CoroutineContext) : Closeable, CoroutineScope {
        override val coroutineContext: CoroutineContext
            get() = context

        override fun close() {
            coroutineContext.cancel()
        }
    }
}