//                          Bismillahirrahmanirrahim


package com.flife.linr.libs

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun post(block: (() -> Unit)) = withContext(Dispatchers.Main) {
    block()
}

suspend fun postImmediately(block: (() -> Unit)) = withContext(Dispatchers.Main.immediate) {
    block()
}

suspend fun <T> post(receiver: T, block: (T.() -> Unit)) = withContext(Dispatchers.Main) {
    receiver.block()
}

suspend fun <T> postImmediately(receiver: T, block: (T.() -> Unit)) = withContext(Dispatchers.Main.immediate) {
    receiver.block()
}