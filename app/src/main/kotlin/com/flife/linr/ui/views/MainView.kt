//                              Bismillahirrahmanirrahim


package com.flife.linr.ui.views

import com.flife.linr.LinRApplication
import com.flife.linr.extensions.*
import com.flife.linr.libs.View
import com.flife.linr.libs.post
import com.flife.linr.models.Case
import com.flife.linr.viewmodels.MainViewModel
import javafx.collections.ListChangeListener
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.scene.layout.HBox
import javafx.util.converter.DoubleStringConverter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainView : View("Main") {

    override val root: HBox by fxml()
    private val viewModel: MainViewModel by inject()
    private val tableView: TableView<Case> by fxid()
    private val clearCaseButton: Button by fxid()
    private val removeCaseButton: Button by fxid()
    private val xTextField: TextField by fxid()
    private val yTextField: TextField by fxid()
    private val inputCaseButton: Button by fxid()
    private val sigmaXLabel: Label by fxid()
    private val sigmaXSquareLabel: Label by fxid()
    private val sigmaXYLabel: Label by fxid()
    private val sigmaYLabel: Label by fxid()
    private val sigmaYSquareLabel: Label by fxid()
    private val nLabel: Label by fxid()
    private val aLabel: Label by fxid()
    private val bLabel: Label by fxid()
    private val predictionTableView: TableView<Case> by fxid()
    private val xPredictionTextField: TextField by fxid()
    private val yPredictionTextField: TextField by fxid()
    private val predictButton: Button by fxid()
    private val inputPredictionButton: Button by fxid()
    private val removePredictionButton: Button by fxid()
    private val clearPredictionButton: Button by fxid()
    private val progressBar: ProgressBar by fxid()
    private var isClearOtherPrediction = true

    init {
        viewScope.launch(Dispatchers.Default) {
            showProgressBar()
            initialization()
            hideProgressBar()
        }
    }

    override fun onBeforeShow() {
        super.onBeforeShow()
        val icon = LinRApplication.ICON.image
        currentStage?.icons?.add(icon)
    }

    private suspend fun initialization() {
        tableViewsInit()
        buttonsInit()
        textFieldsInit()
    }

    private suspend fun tableViewsInit() {
        val converter = DoubleStringConverter()
        val tableCell =
            TextFieldTableCell.forTableColumn<Case, Double>(converter)

        val xColumn = TableColumn<Case, Double>("X").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("x")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(5))
        }
        val yColumn = TableColumn<Case, Double>("Y").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("y")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(5))
        }

        val xSquareColumn = TableColumn<Case, Double>("X^").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("xSquare")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(5))
        }
        val xyColumn = TableColumn<Case, Double>("XY").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("xy")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(5))
        }
        val ySquareColumn = TableColumn<Case, Double>("Y^").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("ySquare")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(5))
        }

        post(tableView) {
            disableHorizontalScrolling()

            with(columns) {
                add(xColumn)
                add(yColumn)
                add(xSquareColumn)
                add(xyColumn)
                add(ySquareColumn)
            }

            //Set the viewModel's cases first
            //or maybe you will get incorrect a and b value !!!
            viewModel.setCases(tableView.items)
            items.addListener(ListChangeListener {
                renewValueLabels()
            })
        }

        val xPredictionColumn = TableColumn<Case, Double>("X").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("x")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(2))
        }
        val yPredictionColumn = TableColumn<Case, Double>("Y").apply {
            cellFactory = tableCell
            cellValueFactory = PropertyValueFactory("y")
            prefWidthProperty().bind(this@MainView.tableView.widthProperty().divide(2))
        }

        post(predictionTableView) {
            disableHorizontalScrolling()

            with(columns) {
                add(xPredictionColumn)
                add(yPredictionColumn)
            }
        }
    }

    private fun buttonsInit() {
        clearCaseButton.setOnAction {
            viewModel.clearCases()
        }

        removeCaseButton.setOnAction {
            showProgressBar()

            viewScope.launch(Dispatchers.Default) {
                val selectedItems = tableView
                    .selectionModel
                    .selectedItems

                post { viewModel.removeCases(selectedItems) }
                hideProgressBar()
            }
        }

        inputCaseButton.setOnAction {inputCase() }

        predictButton.setOnAction { predictCase() }

        with(inputPredictionButton) {
            disable()
            setOnAction { inputPredictionCase() }
        }

        removePredictionButton.setOnAction {
            showProgressBar()

            viewScope.launch(Dispatchers.Default) {
                val selectedItems = predictionTableView
                    .selectionModel
                    .selectedItems

                post {
                    predictionTableView.items.removeAll(selectedItems)
                }
                hideProgressBar()
            }
        }

        clearPredictionButton.setOnAction {
            predictionTableView.items.clear()
        }
    }

    private fun textFieldsInit() {
        val regex = Regex("-?[0-9]*\\.?[0-9]*")

        with(xTextField) {
            onTextChanged { _, oldValue, newValue ->
                newValue?.run {
                    if (!matches(regex)) {
                        text = oldValue
                    }
                }
            }
        }

        with(yTextField) {
            onTextChanged { _, oldValue, newValue ->
                newValue?.run {
                    if (!matches(regex)) {
                        text = oldValue
                    }
                }
            }

            setOnAction { inputCase() }
        }

        with(xPredictionTextField) {
            onTextChanged { _, oldValue, newValue ->
                newValue?.run {
                    if (!matches(regex)) {
                        text = oldValue
                    } else {
                        if (isClearOtherPrediction) {
                            yPredictionTextField.clear()
                            text = this

                            inputPredictionButton.disable()
                        }
                    }
                }

                setOnAction { predictCase() }
            }
        }

        with(yPredictionTextField) {
            onTextChanged { _, oldValue, newValue ->
                newValue?.run {
                    if (!matches(regex)) {
                        text = oldValue
                    } else {
                        if (isClearOtherPrediction) {
                            xPredictionTextField.clear()
                            text = this

                            inputPredictionButton.disable()
                        }
                    }
                }

                setOnAction { predictCase() }
            }
        }
    }

    private fun inputCase() {
        disableXYTextField()
        showProgressBar()

        viewScope.launch(Dispatchers.Default) {
            val xText = xTextField.text
            val yText = yTextField.text

            if (xText.isEmpty()) {
                enableXYTextField()
                post { xTextField.requestFocus() }

                if (!xPredictionTextField.isDisabled) hideProgressBar()

                return@launch
            }

            if (yText.isEmpty()) {
                enableXYTextField()
                post { yTextField.requestFocus() }

                if (!xPredictionTextField.isDisabled) hideProgressBar()

                return@launch
            }

            val x = xText.toDouble()
            val y = yText.toDouble()

            val case = Case(x, y)

            post { viewModel.addCase(case) }

            clearXYTextField()
            enableXYTextField()
            post(xTextField) { requestFocus() }
            hideProgressBar()
        }
    }

    private fun predictCase() {
        disableXYPredictionTextField()
        showProgressBar()

        viewScope.launch(Dispatchers.Default) {
            val xText = xPredictionTextField.text
            val yText = yPredictionTextField.text

            if (xText.isEmpty() && yText.isEmpty()) {
                enableXYPredictionTextField()
                post { xPredictionTextField.requestFocus() }

                if (!xTextField.isDisabled) hideProgressBar()

                return@launch
            }

            val case = if (xText.isNotEmpty() && yText.isEmpty()) {
                val x = xText.toDouble()
                val y = viewModel.yOf(x)

                Case(x, y)
            } else {
                val y = yText.toDouble()
                val x = viewModel.xOf(y)

                Case(x, y)
            }

            post {
                isClearOtherPrediction = false

                xPredictionTextField.text = format(case.x!!)
                yPredictionTextField.text = format(case.y!!)
                inputPredictionButton.enable()

                isClearOtherPrediction = true
            }

            viewModel.predictedCase = case

            enableXYPredictionTextField()
            hideProgressBar()
        }
    }

    private fun inputPredictionCase() {
        disableXYPredictionTextField()
        showProgressBar()

        viewScope.launch(Dispatchers.Default) {
            val xText = xPredictionTextField.text
            val yText = yPredictionTextField.text

            val x = xText.toDouble()
            val y = yText.toDouble()

            val case = Case(x, y)

            post {
                predictionTableView.items.add(case)
            }

            clearXYPredictionTextField()
            enableXYPredictionTextField()
            post(xPredictionTextField) { requestFocus() }
            hideProgressBar()
        }
    }

    private fun renewValueLabels() {
        with(viewModel) {
            val sigmaX = sigmaX
            val sigmaXSquare = sigmaXSquare
            val sigmaXY = sigmaXY
            val sigmaY = sigmaY
            val sigmaYSquare = sigmaYSquare
            val n = n
            val a = a
            val b = b

            sigmaXLabel.text = ": ${format(sigmaX)}"
            sigmaXSquareLabel.text = ": ${format(sigmaXSquare)}"
            sigmaXYLabel.text = ": ${format(sigmaXY)}"
            sigmaYLabel.text = ": ${format(sigmaY)}"
            sigmaYSquareLabel.text = ": ${format(sigmaYSquare)}"
            nLabel.text = ": $n"
            aLabel.text = ": ${format(a)}"
            bLabel.text = ": ${format(b)}"
        }
    }

    private fun format(value: Double) = String.format("%.2f", value)

    private suspend fun enableXYTextField() = post {
        xTextField.enable()
        yTextField.enable()
    }

    private fun disableXYTextField() {
        xTextField.disable()
        yTextField.disable()
    }

    private suspend fun clearXYTextField() = post {
        xTextField.clear()
        yTextField.clear()
    }

    private suspend fun enableXYPredictionTextField() = post {
        xPredictionTextField.enable()
        yPredictionTextField.enable()
    }

    private fun disableXYPredictionTextField() {
        xPredictionTextField.disable()
        yPredictionTextField.disable()
    }

    private fun clearXYPredictionTextField() {
        xPredictionTextField.disable()
        yPredictionTextField.disable()
    }

    private fun showProgressBar() {
        progressBar.visible()
    }

    private suspend fun hideProgressBar() = post {
        progressBar.invisible()
    }
}