//                              Bismillahirrahmanirrahim


package com.flife.linr.utils

import java.io.InputStream

object Resource {

    fun getDrawable(name: String): InputStream {
        val url = "../ui/drawable/${name}"

        return javaClass.getResourceAsStream(url)
    }
}