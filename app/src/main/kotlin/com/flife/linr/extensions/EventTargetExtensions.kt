//                              Bismillahirrahmanirrahim


package com.flife.linr.extensions

import com.flife.linr.utils.Resource
import javafx.event.EventTarget

fun EventTarget.getDrawable(name: String) = Resource.getDrawable(name)