//                              Bismillahirrahmanirrahim


package com.flife.linr.extensions

import javafx.beans.value.ObservableValue
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.input.ScrollEvent

fun TextField.onTextChanged(
    block: ((observable: ObservableValue<out String>?, oldValue: String?, newValue: String?) -> Unit)
) {
    textProperty().addListener(block)
}

fun <T> TreeView<T>.onSelectedItem(
    block: ((observable: ObservableValue<out TreeItem<T>>?, oldValue: TreeItem<T>?, newValue: TreeItem<T>?) -> Unit)
) {
    selectionModel.selectedItemProperty().addListener(block)
}

fun <T> TableView<T>.disableHorizontalScrolling() {
    addEventFilter(ScrollEvent.ANY) {
        if (it.deltaX != 0.0) it.consume()
    }
}