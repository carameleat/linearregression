//                              Bismillahirrahmanirrahim


package com.flife.linr.extensions

import javafx.scene.Node

fun Node.visible() {
    isVisible = true
}

fun Node.invisible() {
    isVisible = false
}

fun Node.enable() {
    isDisable = false
}

fun Node.disable() {
    isDisable = true
}