//                              Bismillahirrahmanirrahim


package com.flife.linr.extensions

import javafx.stage.Stage

fun Stage.resizable() {
    isResizable = true
}

fun Stage.unResizable() {
    isResizable = false
}