//                                  Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm")
    kotlin("kapt")
    id("org.openjfx.javafxplugin")
}

group = "com.flife"
version = "1.0-SNAPSHOT"

application {
    mainClassName = "com.flife.linr.LinRApplication"
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "11"
    }
}

javafx {
    version = "15"
    modules = listOf("javafx.controls", "javafx.fxml")
}


dependencies {
    val tornadoFxVersion = "1.7.20"

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("no.tornado:tornadofx:$tornadoFxVersion")
    
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.4.1")

    testImplementation("junit", "junit", "4.12")
}
